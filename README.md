# Design decisions #

## 5 end points: ##
- create customer (HTTP.POST)
- update customer (HTTP.PUT)
- get customers (list) (HTTP.GET)
- get customer by customer id (HTTP.GET)
- delete customer (HTTP.DELETE)
  
## Considerations ##

### flows and testability ###
- flows are divided into interface vs implementation abstraction layers
- key flow elements are abstracted into their own flows for better testability
- request-response exchange pattern applicable in this case
- caching could be considered for customer listings as the functionality is reasonably resource intensive (out of scope for this test)
- queued asynchronous flow processing strategy could be considered for non transactional endpoints to improve performance e.g. on HTTP GET's
- logging at key steps (capture request payload, output payload with message id) (out of scope for this test)
- exception handling flows - dedicated flows provided as a separate customers.exception.xml

### message body ###
- message id and customer id as system generated guids for security considerations, never expose sequential or guessable id's to consumers - this provides another layer of abstraction, it's ok to correlate these id's to id's internal to the system
- clientId in the header for identification auditing purposes
- authorization header (JWT) usually considered for security purposes but not applicable in this case
- message id (for auditing and logging purposes)
- datetime updated (for syncing purposes)  
- HAL (for user experience i.e. nagivation purposes - _links only, _embedded not relevant as there are no embedded resources in this case)

- returning the full message body with 
```

{
    "messageId": "dfbec0a0-b269-11e7-8a49-e4b318c04c93",
    "success": true,
    "customer": {
        "customerId": "05998973-afce-4df0-b43b-2dbcc5f478b1",
        "firstName": "david",
        "lastName": "test",
        "address1": "street 1",
        "address2": "street 2",
        "lastUpdated": "2017-10-17 01:02:25+1300",
        "_links": {
            "self": {
                "href": "/api/customers/05998973-afce-4df0-b43b-2dbcc5f478b1"
            }
        }
    }
}
```

### message validation ###
- error handling for invalid message format e.g. JSON schema validation failure:
```
HTTP.400
{
    "success": false,
    "errors": [
        {
            "code": "400GEN",
            "source": "",
            "message": "Bad Request",
            "details": "Error validating JSON. Error: {error: instance failed to match all required schemas (matched only 1 out of 2)    level: 'error'    schema: {'loadingURI':'customer.request.post.schema.json#','pointer':''}    instance: {'pointer':''}    domain: 'validation'    keyword: 'allOf'    matched: 1    nrSchemas: 2    reports: {'/allOf/0':[],'/allOf/1':[{'level':'error','schema':{'loadingURI':'customer.request.post.schema.json#','pointer':'/allOf/1'},'instance':{'pointer':''},'domain':'validation','keyword':'required','message':'object has missing required properties ([ 'lastName '])','required':['address1','address2','firstName','lastName'],'missing':['lastName']}]}} Please contact the support team at help@deloitte.co.nz"
        }
    ],
    "messageId": "1d3ee340-b2a8-11e7-8a49-e4b318c04c93"
}
```
- resource not found
```
HTTP.404
{
    "success": false,
    "errors": [
        {
            "code": "404GEN",
            "source": "",
            "message": "Not Found",
            "details": "The resource you are looking for does not exist, please consult the API documentation if in doubt. Please contact the support team at help@deloitte.co.nz"
        }
    ],
    "messageId": "54de63c0-b2a8-11e7-8a49-e4b318c04c93"
}
```

### create customer (HTTP.POST) ###
- mandatory fields > first name, last name and addresses in POST body
- request
```
http://127.0.0.1:8081/api/customers

{
  "firstName": "david",
  "lastName": "test",
  "address1": "street 1",
  "address2": "street 2"
}
```
- message returned
```
HTTP.201
{
    "messageId": "dfbec0a0-b269-11e7-8a49-e4b318c04c93",
    "success": true,
    "customer": {
        "customerId": "05998973-afce-4df0-b43b-2dbcc5f478b1",
        "firstName": "david",
        "lastName": "test",
        "address1": "street 1",
        "address2": "street 2",
        "lastUpdated": "2017-10-17 01:02:25+1300",
        "_links": {
            "self": {
                "href": "/api/customers/05998973-afce-4df0-b43b-2dbcc5f478b1"
            }
        }
    }
}
```

### list customers (HTTP.GET) ###
- mandatory maxResults query parameter , 0> return all results 2> return 2 results
- queryFilter would be considered but not relevant in this test
- resultCount returned to aide consumer programming efforts
- request
```
http://127.0.0.1:8081/api/customers?maxResults=0
```
- message returned
```
{
    "messageId": "e5b9f8d0-b269-11e7-8a49-e4b318c04c93",
    "success": true,
    "resultCount": 3,
    "customers": [
        {
            "customerId": "ccddfe9e-01dc-48f8-9e6c-cf1afd47e15c",
            "lastUpdated": "2017-10-17 01:02:23+1300",
            "_links": {
                "self": {
                    "href": "/api/customers/ccddfe9e-01dc-48f8-9e6c-cf1afd47e15c"
                }
            }
        },
        {
            "customerId": "05998973-afce-4df0-b43b-2dbcc5f478b1",
            "lastUpdated": "2017-10-17 01:02:25+1300",
            "_links": {
                "self": {
                    "href": "/api/customers/05998973-afce-4df0-b43b-2dbcc5f478b1"
                }
            }
        },
        {
            "customerId": "5745d962-b796-47d3-94c2-0b3a519b7f91",
            "lastUpdated": "2017-10-17 01:02:24+1300",
            "_links": {
                "self": {
                    "href": "/api/customers/5745d962-b796-47d3-94c2-0b3a519b7f91"
                }
            }
        }
    ]
}
```

### get & delete customer by customerId (HTTP.GET, DELETE) ###
- mandatory customer id 
- request
```
http://127.0.0.1:8081/api/customers/05998973-afce-4df0-b43b-2dbcc5f478b1
```
- message returned
```
HTTP.200
{
    "messageId": "dfbec0a0-b269-11e7-8a49-e4b318c04c93",
    "success": true,
    "customer": {
        "customerId": "05998973-afce-4df0-b43b-2dbcc5f478b1",
        "firstName": "david",
        "lastName": "test",
        "address1": "street 1",
        "address2": "street 2",
        "lastUpdated": "2017-10-17 01:02:25+1300",
        "_links": {
            "self": {
                "href": "/api/customers/05998973-afce-4df0-b43b-2dbcc5f478b1"
            }
        }
    }
}
```

### update customer (HTTP.PUT) ###
- mandatory fields customer id as an uri parameter > first name, last name and addresses in POST body
- request
```
http://127.0.0.1:8081/api/customers/05998973-afce-4df0-b43b-2dbcc5f478b1

{
  "firstName": "david",
  "lastName": "test",
  "address1": "street 1",
  "address2": "street 2"
}
```
- message returned
```
HTTP.200
{
    "messageId": "dfbec0a0-b269-11e7-8a49-e4b318c04c93",
    "success": true,
    "customer": {
        "customerId": "05998973-afce-4df0-b43b-2dbcc5f478b1",
        "firstName": "david",
        "lastName": "test",
        "address1": "street 1",
        "address2": "street 2",
        "lastUpdated": "2017-10-17 01:02:25+1300",
        "_links": {
            "self": {
                "href": "/api/customers/05998973-afce-4df0-b43b-2dbcc5f478b1"
            }
        }
    }
}
```

## POSTMAN test link: ##
```
https://www.getpostman.com/collections/6f5b4f0ed5f71e5f0565
```


