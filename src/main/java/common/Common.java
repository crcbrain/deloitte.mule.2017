package common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;

import org.hamcrest.core.IsInstanceOf;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.ApikitRuntimeException;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.module.apikit.exception.NotFoundException;
import org.mule.module.http.api.HttpConstants;
import org.mule.util.CaseInsensitiveHashMap;
import org.mule.util.IOUtils;
import org.opensaml.ws.wssecurity.Created;
import org.springframework.http.HttpStatus;

public class Common
{
	public static final String HTTP_CREATED = "201";
	public static final String HTTP_OK = "200";
	public static final String HTTP_UNAUTHENTICATED = "401";
	public static final String HTTP_FORBIDDEN = "403";
	public static final String HTTP_BADREQUEST = "400";
	public static final String HTTP_INTERNALSERVERERROR = "500";
	public static final String HTTP_STATUS = "http.status";
	public static final String AUTH_TYPE_BASIC = "Basic";
	public static final String AUTH_TYPE_BEARER = "Bearer";
	
	public static final String SESSION_HTTP_401_WWW_AUTH_HEADER = "wwwAuthHeader";
	
	public static String getAppProperty(String propertyName, String defaultValue, Class classType)
	{
		
		String valueToReturn = defaultValue;
		try
		{
			Properties prop = new Properties();
		    prop.load(classType.getClassLoader().getResourceAsStream("mule-app.properties"));
		    String environment = prop.get("mule.env").toString();
		
		    prop = new Properties();
		    prop.load(classType.getClassLoader().getResourceAsStream(String.format("%s.properties",environment)));
		    
		    valueToReturn = prop.get(propertyName).toString();
		}
		catch(Exception ex)
		{
		}
		
		return valueToReturn;
	}
	
	public static ArrayList<String> returnAsArrayList(Object result, String splitChars)
	{
		ArrayList<String> results = new ArrayList<String>();
		try
		{
			if(result instanceof String)
				results.addAll(Arrays.asList(((String) result).split(splitChars)));
			else if(result instanceof ArrayList<?>)
				results = (ArrayList<String>)result;
		}
		catch(Exception ex)
		{
			String s = ex.getMessage();
		}
		return results;
	}
	
	public static Object getFieldValue(Object result, String fieldName, String defaultValue)
    {
    	Object valueToReturn = defaultValue;
    	
    	if(result instanceof HashMap)
        {
    		valueToReturn =  ((HashMap) result).get(fieldName);
        }
        else if(result instanceof CaseInsensitiveHashMap)
        {
        	valueToReturn =  ((CaseInsensitiveHashMap) result).get(fieldName);
        }
    	
    	return valueToReturn;
    }
    
	private static void setErrorProperties(String errorMessage,  String errorDetail, String sourceSystem,MuleMessage message)
	{
		message.setProperty("errorDetails",  String.format("%s:%s",errorMessage,errorDetail),PropertyScope.SESSION);
		message.setProperty("sourceSystem", sourceSystem,PropertyScope.SESSION);
	}
    
	public static void throwBadRequest(String errorMessage,  String errorDetail, String sourceSystem,MuleMessage message) throws BadRequestException{
		setErrorProperties(errorMessage,errorDetail,sourceSystem,message);
		throw new org.mule.module.apikit.exception.BadRequestException(String.format("%s,%s",errorMessage,errorDetail));
    }
	
	public static void throwNotFoundException(String errorMessage,  String errorDetail, String sourceSystem,MuleMessage message) throws NotFoundException{
		setErrorProperties(errorMessage,errorDetail,sourceSystem,message);
		throw new org.mule.module.apikit.exception.NotFoundException(String.format("%s,%s",errorMessage,errorDetail));
    }
    
	public static void throwInternalServerError(String errorMessage,  String errorDetail, String sourceSystem,MuleMessage message) throws ApikitRuntimeException{
		setErrorProperties(errorMessage,errorDetail,sourceSystem,message);
		throw new org.mule.module.apikit.exception.ApikitRuntimeException(String.format("%s,%s",errorMessage,errorDetail));
    }
}