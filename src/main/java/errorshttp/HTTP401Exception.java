package errorshttp;

import common.Common;

public class HTTP401Exception extends Exception
{
	public static final String ERROR_INVALID_REQUEST = "invalid_request";
	public static final String ERROR_INVALID_TOKEN = "invalid_request";
	public static final String ERROR_INSUFFICIENT_SCOPE = "insufficient_scope";
	
  public HTTP401Exception (String message)
  {
    super(message);
  }
  
  public static String getWWWAuthenticateHeader(String authType, String error, String errorDescription,String scope, Class classType)
  {
	  String realm = Common.getAppProperty("api.realm","api.deloittest.co.nz",classType);
	  String header =  Common.getAppProperty("template.error.401.header.wwwauthenticate", "{AUTHTYPE} realm=\"{REALM}\",error=\"{ERROR}\",error_description=\"{ERRORDESC}\",scope=\"{SCOPE}\"",classType);
	  
	  return header.replace("{AUTHTYPE}",authType).replace("{REALM}",realm).replace("{ERROR}",error).replace("{ERRORDESC}",errorDescription).replace("{SCOPE}",scope);
  }
}
