package errorshttp;

public class HTTP403Exception extends Exception
{
  public HTTP403Exception (String message)
  {
    super(message);
  }
}
